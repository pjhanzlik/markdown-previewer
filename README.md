This is a tribute page made as coursework for freeCodeCamp's Front End Libraries
Developer Certification. In addition to fulfilling
[required user stories](https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-markdown-previewer),
I wanted to emulate the 
[example freeCodeCamp web page](https://codepen.io/freeCodeCamp/full/GrZVVO)
styling with CSS queries which scale down to
[240px width devices](https://www.kaiostech.com/meet-the-devices-that-are-powered-by-kaios/).

CSS variables allow JavaScript easy access to manipulate the styling of
multiple elements at once.  In addition, React Components isolate complicated
code like inline SVGs, custom button scripts, and asynchronous behavior into
easily manageble chunks.

This site has been manually tested to render nicely on mobile and desktop
[WebKit](https://webkit.org/), [Blink](https://www.chromium.org/blink), and
[Servo](https://servo.org/) powered browsers, but your milage may vary.
If you do spot a rendering problem, please
[create a new Issue](https://gitlab.com/pjhanzlik/markdown-previewer/issues)
with a screenshot of the problem and a description of your hardware.