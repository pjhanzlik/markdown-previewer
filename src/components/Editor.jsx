import React from 'react';

export default function Editor(props) {
    return (
        <textarea
            className="window-body"
            id={props.name}
            name={props.name}
            onChange={props.onChange}
            value={props.value}
        />
    );
}