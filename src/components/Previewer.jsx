import React from 'react';

export default function Previewer(props) {
    return (
        <output
            className="window-body"
            id="preview"
            name={props.name}
            dangerouslySetInnerHTML={props.dangerouslySetInnerHTML}
        />
    );
} 