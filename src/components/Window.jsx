import React, {useState} from 'react';
import ResizeButton from './ResizeButton';

export default function Window(props) {
    const [max, setMax] = useState('');

    function handleClick() {
        if(max === '') {
            setMax('window-max');
        }
        else {
            setMax('');
        }
    }

    return (
        <article className={`window ${max} window-${props.size}`} >
            <header className="window-header">
                <label htmlFor={props.name}>{props.name}</label>
                <ResizeButton
                    onClick={handleClick}
                    height="1.5em"
                    width="1.5em"
                />
            </header>
            {props.children}
        </article>
    );
} 