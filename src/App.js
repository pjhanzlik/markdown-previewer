import React, { useState } from 'react';
import Marked from 'marked';
import DOMPurify from 'dompurify'

import './App.css';
import Editor from './components/Editor';
import Previewer from './components/Previewer'
import Window from './components/Window';

Marked.setOptions({breaks: true});
const WELCOME_TEXT = `# This is a simple React Markdown previewer
  
To use [this program](https://pjhanzlik.gitlab.io/markdown-previewer) simply:
1. Write in the **Editor** above and...
2. Watch content get filled out *below*

You can write code like \`this\`, or even like...

\`\`\`
/*
this! ... is a block comment...
*/
\`\`\`

## Here's a cat picture to finish things off

![Tiger](https://svgsilh.com/svg/1295198.svg)
> A tiger is totally a cat [citation needed]
`;

export default function App(props){
  const [text, setText] = useState(WELCOME_TEXT);

  function handleChange(e) {
    setText(e.target.value);
  }

  function createMarkup() {
    return {__html: DOMPurify.sanitize(Marked(text))};
  }

  return (
    <form className="App">
      <Window
        name="editor"
        size="small" 
      >
        <Editor
          onChange={handleChange}
          value={text}
        />
      </Window>
      <Window
        name="previewer"
      >
        <Previewer
          dangerouslySetInnerHTML={createMarkup()}
        />
      </Window>
    </form>
  );
}