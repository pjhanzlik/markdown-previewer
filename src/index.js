import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// Uncomment the following segment to run FCC tests
/*
const TEST = document.createElement("script");
TEST.setAttribute('src', 'https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js');
document.body.appendChild(TEST);
*/